from enum import Enum


class Commands(Enum):
    A_COMMAND = 'A'
    C_COMMAND = 'C'
    L_COMMAND = 'L'
