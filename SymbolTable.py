class SymbolTable:
    def __init__(self):
        self.__table = {}

    def add_entry(self, symbol: str, address: int):
        self.__table[symbol] = address

    def contains(self, symbol: str):
        return symbol in self.__table

    def get_address(self, symbol: str) -> int:
        return self.__table[symbol]