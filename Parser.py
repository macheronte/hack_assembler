from Commands import Commands
import re


class Parser:

    def __init__(self, file_path: str):
        file = open(file_path, "r", encoding="utf-8")

        self.__dest = None
        self.__comp = None
        self.__jump = None
        self.__symbol = None
        self.__type = None

        self.__lines = []

        if file.readable():
            for line in file:
                parts = line.split(r'//')

                if len(parts) > 1:
                    line = parts[0]

                line = line.replace(" ", "")
                line = line.replace("\n", "")

                if line is not '':
                    self.__lines.append(line)

            file.close()

            self.__current_index_command = -1
        else:
            raise Exception('File is not readable.')

    def has_more_commands(self) -> bool:
        return self.__current_index_command < len(self.__lines) - 1

    def advance(self):
        if self.has_more_commands():
            self.__current_index_command = self.__current_index_command + 1
            self.__parse_command(self.__lines[self.__current_index_command])
        else:
            raise Exception('No more commands are available')

    def reset(self):
        self.__current_index_command = -1

    def __parse_command(self, command):
        self.__dest = None
        self.__comp = None
        self.__jump = None
        self.__type = None
        self.__symbol = None

        # parsing @variable
        matches = re.match(r'@[a-zA-Z_.$:][a-zA-Z0-9_.$:]*', command)

        if matches is not None:
            self.__type = Commands.A_COMMAND
            self.__symbol = matches.group().replace("@", "")
            return

        # parsing (VARIABLE)
        matches = re.match(r'\([a-zA-Z_.$:][a-zA-Z0-9_.$:]*\)', command)

        if matches is not None:
            self.__type = Commands.L_COMMAND
            self.__symbol = matches.group().replace("(", "").replace(")", "")
            return

        # parsing @100
        matches = re.match(r'@[0-9]+', command)

        if matches is not None:
            self.__type = Commands.A_COMMAND
            self.__symbol = matches.group().replace("@", "")
            return

        # parsing M=A+1;JMP
        matches = re.match(
            r'((^AMD|^AD|^AM|^MD|^A|^D|^M)=|)(0|1|-1|D\+1|A\+1|M\+1|D-1|A-1|M-1|D\+A|D\+M|D-A|D-M|A-D|M-D|'
            r'D&A|D&M|D\|A|D\|M|D|A|M|!D|!A|!M|-D|-A|-M)(;(JGT|JEQ|JGE|JLT|JNE|JLE|JMP)|)', command)

        if matches is not None:
            self.__type = Commands.C_COMMAND
            self.__dest = matches.groups()[1]
            self.__comp = matches.groups()[2]
            self.__jump = matches.groups()[4]
            return

        raise Exception("Instruction not valid: " + str(command))

    def command_type(self) -> Commands:
        return self.__type

    def symbol(self) -> str:
        return self.__symbol

    def dest(self) -> str:
        return self.__dest

    def comp(self) -> str:
        return self.__comp

    def jump(self) -> str:
        return self.__jump
