from Commands import Commands
from Parser import Parser
from Code import Code
from SymbolTable import SymbolTable

file = r'path\to\nand2tetris\projects\06\pong\Pong'

parser = Parser(file + '.asm')

table = SymbolTable()
binary_code = []
instruction_index = 0
variable_address = 16

table.add_entry("SP", 0)
table.add_entry("LCL", 1)
table.add_entry("ARG", 2)
table.add_entry("THIS", 3)
table.add_entry("THAT", 4)
table.add_entry("R0", 0)
table.add_entry("R1", 1)
table.add_entry("R2", 2)
table.add_entry("R3", 3)
table.add_entry("R4", 4)
table.add_entry("R5", 5)
table.add_entry("R6", 6)
table.add_entry("R7", 7)
table.add_entry("R8", 8)
table.add_entry("R9", 9)
table.add_entry("R10", 10)
table.add_entry("R11", 11)
table.add_entry("R12", 12)
table.add_entry("R13", 13)
table.add_entry("R14", 14)
table.add_entry("R15", 15)
table.add_entry("SCREEN", 16384)
table.add_entry("KBD", 24576)

while parser.has_more_commands():
    parser.advance()

    if parser.command_type() is Commands.L_COMMAND:
        symbol = parser.symbol()

        if not table.contains(symbol):
            table.add_entry(symbol, instruction_index)
    else:
        instruction_index = instruction_index + 1

parser.reset()

while parser.has_more_commands():
    parser.advance()

    if parser.command_type() is Commands.A_COMMAND:
        symbol = parser.symbol()

        if not symbol.isnumeric():
            if not table.contains(symbol):
                table.add_entry(symbol, variable_address)
                variable_address = variable_address + 1
            symbol = table.get_address(symbol)

        binary_code.append('0' + str(f'{int(symbol):015b}'))
    elif parser.command_type() is Commands.C_COMMAND:
        binary_code.append('111' + Code.comp(parser.comp()) + Code.dest(parser.dest()) + Code.jump(parser.jump()))

    if parser.command_type() is not Commands.L_COMMAND:
        binary_code.append('\n')

f = open(file + '.hack', "w")
f.writelines(binary_code)