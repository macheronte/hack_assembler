class Code:

    @staticmethod
    def dest(mnemonic: str) -> str:
        d1 = d2 = d3 = 0

        if mnemonic is not None:
            if 'M' in mnemonic:
                d3 = 1

            if 'D' in mnemonic:
                d2 = 1

            if 'A' in mnemonic:
                d1 = 1

        return str(d1) + str(d2) + str(d3)

    @staticmethod
    def comp(mnemonic: str) -> str:
        a = c1 = c2 = c3 = c4 = c5 = c6 = 0

        a_comp = ['M', '!M', '-M', 'M+1', 'M-1', 'D+M', 'D-M', 'M-D', 'D&M', 'D|M']
        c1_comp = ['0', '1', '-1', 'A', 'M', '!A', '!M', '-A', '-M', 'A+1', 'M+1', 'A-1', 'M-1']
        c2_comp = ['1', '-1', 'A', 'M', '!A', '!M', '-A', '-M', 'D+1', 'A+1', 'M+1', 'A-1', 'M-1', 'D-A', 'D-M',
                   'D|A', 'D|M']
        c3_comp = ['0', '1', '-1', 'D', '!D', '-D', 'D+1', 'D-1']
        c4_comp = ['1', 'D', '!D', '-D', 'D+1', 'A+1', 'M+1', 'D-1', 'A-D', 'M-D', 'D|A', 'D|M']
        c5_comp = ['0', '1', '-1', '-D', '-A', '-M', 'D+1', 'A+1', 'M+1', 'D-1', 'A-1', 'M-1', 'D+A', 'D+M', 'D-A',
                   'D-M', 'A-D', 'M-D']
        c6_comp = ['1', '!D', '!A', '!M', '-D', '-A', '-M', 'D+1', 'A+1', 'M+1', 'D-A', 'D-M', 'A-D', 'M-D',
                   'D|A', 'D|M']

        if mnemonic in a_comp:
            a = 1

        if mnemonic in c1_comp:
            c1 = 1

        if mnemonic in c2_comp:
            c2 = 1

        if mnemonic in c3_comp:
            c3 = 1

        if mnemonic in c4_comp:
            c4 = 1

        if mnemonic in c5_comp:
            c5 = 1

        if mnemonic in c6_comp:
            c6 = 1

        return str(a) + str(c1) + str(c2) + str(c3) + str(c4) + str(c5) + str(c6)

    @staticmethod
    def jump(mnemonic: str) -> str:
        j1 = j2 = j3 = 0

        j3_jump = ['JGT', 'JGE', 'JNE', 'JMP']
        j2_jump = ['JEQ', 'JGE', 'JLE', 'JMP']
        j1_jump = ['JLT', 'JNE', 'JLE', 'JMP']

        if mnemonic is not None:
            if mnemonic in j3_jump:
                j3 = 1

            if mnemonic in j2_jump:
                j2 = 1

            if mnemonic in j1_jump:
                j1 = 1

        return str(j1) + str(j2) + str(j3)